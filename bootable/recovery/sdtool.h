/*
 * sdtool.h
 *
 *  Created on: 2014-3-20
 *      Author: mmk
 */

#ifndef SDTOOL_H_
#define SDTOOL_H_
#include <map>
#include <string>
#include <sstream>

using namespace std;

typedef struct {
	char *name;
	char *value;
}RKSdBootCfgItem;

typedef map<string,string> MAP_SD_CONFIG;
typedef MAP_SD_CONFIG::iterator sd_config_map_iter;



#endif /* SDTOOL_H_ */
